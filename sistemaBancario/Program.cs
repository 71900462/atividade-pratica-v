﻿using System;

namespace sistemaBancario
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcao = 1;
            Conta c = new Conta();
            Console.WriteLine("------------------------------");
            Console.WriteLine("GESTÃO BANCÁRIA");
            Console.WriteLine("------------------------------");
            Conta.CriaConta(c);
            Console.WriteLine("------------------------------");
            Console.WriteLine("Bem vindo, " + c.NomeTitular + ".");
            Console.WriteLine("Conta: " + c.NumConta + " -  Agência: " + c.NumAgencia + "\nTipo de Conta: " + c.TipoConta);
        }

        while (opcao != 0)
        {
            Console.WriteLine("------------------------------");
            Console.WriteLine("Escolha uma das opções: ");
            Console.WriteLine("1. Consultar Saldo");
            Console.WriteLine("2. Sacar");
            Console.WriteLine("3. Depositar");
            Console.WriteLine("0. Sair do sistema");
            opcao = Convert.ToInt32(Console.ReadLine());

            switch (opcao)
            {
                case 1:
                    Conta.ConsultaSaldo(c);
                    break;
                case 2:
                    Conta.SacaSaldo(c);
                    break;
                case 3:
                    Conta.DepositaSaldo(c);
                    break;
                case 0:
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("Obrigado por utilizar nosso sistema, volte sempre!");
                    Console.WriteLine("------------------------------");
                    break;
                default:
                    Console.WriteLine("------------------------------");
                    Console.WriteLine("> ALERTA: Você digitou uma opção inválida! Por favor escolha uma das opções informadas.");
                    break;
            }
        }
    }
}
