using System;

class Conta
{
    //VARIAVEIS
    private string nomeTitular, cpfTitular, tipoConta;
    private double saldoConta;
    private int numConta, numAgencia;

    //GETSET
    public string NomeTitular
    {
        get { return nomeTitular; }
        set { nomeTitular = value; }
    }
    public string TipoConta
    {
        get { return tipoConta; }
        set { tipoConta = value; }
    }
    public string CpfTitular
    {
        get { return cpfTitular; }
        set { cpfTitular = value; }
    }
    public double SaldoConta
    {
        get { return saldoConta; }
        set { saldoConta = value; }
    }
    public int NumConta
    {
        get { return numConta; }
        set { numConta = value; }
    }
    public int NumAgencia
    {
        get { return numAgencia; }
        set { numAgencia = value; }
    }


    public static void CriaConta(Conta c)
    {
        int loopTipoC = 1;
        //CRIAÇÃO DE CONTA
        Random geraNum = new Random();
        while (loopTipoC != 0)
        {
            Console.WriteLine("Qual tipo de conta deseja abrir?\n 1 - Corrente\n 2 - Poupança");
            string codConta = Console.ReadLine();
            if (codConta == "1")
            {
                c.TipoConta = "Corrente";
                loopTipoC = 0;
            }
            else if (codConta == "2")
            {
                c.TipoConta = "Poupança";
                loopTipoC = 0;
            }
            else
            {
                Console.WriteLine("------------------------------");
                Console.WriteLine("> Opção inexistente! Tente novamente.");
                Console.WriteLine("------------------------------");
            }
        }

        Console.WriteLine("------------------------------");
        Console.WriteLine("Digite seu nome:");
        c.NomeTitular = Console.ReadLine();
        Console.WriteLine("Digite seu CPF:");
        c.CpfTitular = Console.ReadLine();
        c.SaldoConta = 0;
        c.NumConta = geraNum.Next(0001, 9999);
        c.NumAgencia = geraNum.Next(0001, 9999);

    }

    public static void SacaSaldo(Conta c)
    {
        Console.WriteLine("------------------------------");
        Console.WriteLine("Digite a quantia que deseja sacar:");
        double saque = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("------------------------------");

        if (c.SaldoConta < saque)
        {
            Console.WriteLine("> Saldo insufisciente.");
        }
        else if (c.SaldoConta == 0)
        {
            Console.WriteLine("> Você não possui saldo.");
        }
        else
        {
            c.SaldoConta = c.SaldoConta - saque;
            Console.WriteLine("> Você sacou a quantia de: R$" + saque + "\nSeu saldo é: R$" + c.SaldoConta);
        }
    }

    public static void DepositaSaldo(Conta c)
    {
        Console.WriteLine("------------------------------");
        Console.WriteLine("Digite a quantidade que deseja depositar:");
        double deposito = Convert.ToDouble(Console.ReadLine());
        c.SaldoConta = c.SaldoConta + deposito;
        Console.WriteLine("------------------------------");
        Console.WriteLine("> Deposito efetuado com sucesso! " + "\nSeu saldo é: R$" + c.SaldoConta);
    }

    public static void ConsultaSaldo(Conta c)
    {
        Console.WriteLine("------------------------------");
        Console.WriteLine("> Seu saldo total é de: R$" + c.SaldoConta);
    }
}